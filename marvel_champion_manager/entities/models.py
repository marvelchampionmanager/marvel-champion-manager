from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Match(models.Model):
    OutcomeTypes = models.TextChoices('outcome', ['Victory', 'Defeat'])
    owner = models.ForeignKey('Profile', related_name='matches', on_delete=models.CASCADE)
    date = models.DateField()
    outcome = models.CharField(choices=OutcomeTypes.choices, max_length=7)
    heroes = models.ManyToManyField('cards.Hero')
    scenario = models.ForeignKey('cards.Scenario', on_delete=models.RESTRICT)
    encounter_sets = models.ManyToManyField('cards.EncounterSet')
    comment = models.TextField(null=True, max_length=100)


class Profile(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(owner=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Collection(models.Model):
    owner = models.OneToOneField('Profile', related_name='collection', on_delete=models.CASCADE)
    hero_collection = models.ManyToManyField('cards.Hero')
    scenario_collection = models.ManyToManyField('cards.Scenario')
    encounterset_collection = models.ManyToManyField('cards.EncounterSet')

    @receiver(post_save, sender=Profile)
    def create_profile_collection(sender, instance, created, **kwargs):
        if created:
            Collection.objects.create(owner=instance)

    @receiver(post_save, sender=Profile)
    def save_profile_collection(sender, instance, **kwargs):
        instance.collection.save()
