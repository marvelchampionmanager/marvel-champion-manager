from rest_framework import serializers
from entities.models import Match, Profile, Collection
from cards.serializers import NameBasedRelatedField
from cards.models import Hero, Scenario, EncounterSet
from rest_framework_nested.relations import NestedHyperlinkedRelatedField
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer


class CustomNestedNameserverSerializer(NestedHyperlinkedModelSerializer):
    def build_url_field(self, field_name, model_class):
        field_class, field_kwargs = super().build_url_field(
            field_name, model_class)
        if hasattr(self, 'url_view_name'):
            field_kwargs['view_name'] = self.url_view_name
        return field_class, field_kwargs


class MatchSerializer(CustomNestedNameserverSerializer):
    parent_lookup_kwargs = {
        'owner_pk': 'owner__pk'
    }
    url_view_name = 'profile-match-detail'
    owner = serializers.ReadOnlyField(source='owner.owner.username')
    heroes = NameBasedRelatedField(many=True, view_name='hero-detail', queryset=Hero.objects.all())
    scenario = NameBasedRelatedField(many=False, view_name='scenario-detail', queryset=Scenario.objects.all())
    encounter_sets = NameBasedRelatedField(many=True, view_name='encounterset-detail',
                                           queryset=EncounterSet.objects.all())

    class Meta:
        model = Match
        fields = ['url', 'id', 'owner', 'date', 'outcome', 'heroes', 'scenario', 'encounter_sets', 'comment']

    def validate_heroes(self, value):
        if len(value) > 4 or len(value) < 1:
            raise serializers.ValidationError('Invalid number')
        return value

    def validate(self, data):
        scenario = data.get('scenario')
        expected_number = scenario.encounter_set_number
        encounter_sets = data.get('encounter_sets')
        number = len(encounter_sets)
        for encounter_set in scenario.mandatory_encounter_sets.all():
            if encounter_set not in encounter_sets:
                raise serializers.ValidationError({
                    'encounter_sets': str(encounter_set.name) + ' is mandatory'
                })
        if expected_number != number:
            raise serializers.ValidationError({
                'encounter_sets': 'Expected ' + str(expected_number) + ' ; Got ' + str(number)
            })
        return data


class CollectionSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.owner.username')
    hero_collection = NameBasedRelatedField(many=True, view_name='hero-detail', queryset=Hero.objects.all())
    scenario_collection = NameBasedRelatedField(many=True, view_name='scenario-detail', queryset=Scenario.objects.all())
    encounterset_collection = NameBasedRelatedField(many=True,
                                                    view_name='encounterset-detail',
                                                    queryset=EncounterSet.objects.all())

    class Meta:
        model = Collection
        fields = ['url', 'id', 'owner', 'hero_collection', 'scenario_collection', 'encounterset_collection']


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    matches = NestedHyperlinkedRelatedField(many=True,
                                            view_name='profile-match-detail',
                                            read_only=True,
                                            parent_lookup_kwargs={'owner_pk': 'owner__pk'})
    collection = serializers.HyperlinkedRelatedField(view_name='collection-detail', read_only=True)

    class Meta:
        model = Profile
        fields = ['url', 'id', 'owner', 'matches', 'collection']
