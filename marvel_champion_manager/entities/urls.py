from django.urls import path, include
from entities import views
from rest_framework_nested import routers

# router = routers.DocumentedRouter()
router = routers.SimpleRouter()
router.register(r'profiles', views.ProfileViewSet, basename='profile')

profile_router = routers.NestedSimpleRouter(router, r'profiles', lookup='owner')
profile_router.register(r'matches', views.MatchViewSet, basename='profile-match')
router.register(r'matches', views.MatchListViewSet, basename='matches')

urlpatterns = [
    path('', include(router.urls)),
    path('', include(profile_router.urls)),
    path('profiles/<int:pk>/collection', views.CollectionView.as_view(), name='collection-detail')
]
