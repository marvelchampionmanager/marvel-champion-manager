from rest_framework import routers


class EntitiesRouter(routers.APIRootView):
    """
    This is the root for accessing the personal entities
    """
    pass


class DocumentedRouter(routers.DefaultRouter):
    APIRootView = EntitiesRouter
