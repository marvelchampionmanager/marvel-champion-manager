from rest_framework import viewsets, permissions
from entities.models import Match, Profile, Collection
from entities.serializers import MatchSerializer, ProfileSerializer, CollectionSerializer
from entities.permissions import MatchPermission, CollectionPermission, ProfilePermission
from rest_framework import generics


class MatchViewSet(viewsets.ModelViewSet):
    serializer_class = MatchSerializer
    # permission_classes = [MatchPermission, permissions.IsAuthenticated]

    def get_queryset(self):
        return Match.objects.filter(owner=self.kwargs['owner_pk'])

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user.profile)


class MatchListViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = MatchSerializer
    # permission_classes = [permissions.IsAdminUser]
    queryset = Match.objects.all()


class CollectionView(generics.RetrieveUpdateAPIView):

    serializer_class = CollectionSerializer
    # permission_classes = [CollectionPermission, permissions.IsAuthenticated]

    def get_queryset(self):
        return Collection.objects.filter(owner=self.kwargs['pk'])


class ProfileViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    # permission_classes = [ProfilePermission, permissions.IsAuthenticated]
