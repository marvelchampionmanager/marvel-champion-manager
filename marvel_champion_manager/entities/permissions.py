from rest_framework import permissions


class MatchPermission(permissions.BasePermission):
    """
    Custom permission to only allow owners
    """
    def has_permission(self, request, view):
        if not bool(request.user):
            return False

        if view.action in ['list', 'create']:
            return int(view.kwargs['owner_pk']) == int(request.user.profile.id) or request.user.is_staff
        elif view.action in ['retrieve', 'update', 'partial_update', 'destroy']:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        if not bool(request.user):
            return False

        if view.action in ['retrieve', 'destroy', 'update', 'partial_update']:
            return obj.owner == request.user.profile or request.user.is_staff
        else:
            return False


class CollectionPermission(permissions.BasePermission):
    """
    Custom permission to only allow owners
    """
    def has_object_permission(self, request, view, obj):
        if not bool(request.user):
            return False

        if request.method in ['GET', 'PUT', 'PATCH']:
            return obj.owner == request.user.profile or request.user.is_staff
        else:
            return False


class ProfilePermission(permissions.BasePermission):
    """
    Custom permission to only allow owners to access their profiles
    """
    def has_permission(self, request, view):
        if view.action == 'list':
            return bool(request.user and request.user.is_staff)
        elif view.action == 'create':
            return False
        elif view.action in ['retrieve', 'update', 'partial_update', 'destroy']:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        if not bool(request.user):
            return False

        if view.action in ['retrieve']:
            return obj == request.user.profile or request.user.is_staff
        elif view.action == ['destroy', 'update', 'partial_update']:
            return False
        else:
            return False
