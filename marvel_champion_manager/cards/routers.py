from rest_framework import routers


class CardsRouter(routers.APIRootView):
    """
    This is the root for accessing the cards model
    """
    pass


class DocumentedRouter(routers.DefaultRouter):
    APIRootView = CardsRouter
