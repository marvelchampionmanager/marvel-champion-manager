from django.urls import path, include
from cards import views, routers

router = routers.DocumentedRouter()
router.register(r'heroes', views.HeroViewSet, basename='hero')
router.register(r'encountersets', views.EncounterSetViewSet, basename='encounterset')
router.register(r'scenarios', views.ScenarioViewSet, basename='scenario')
router.register(r'releases', views.ReleaseViewSet, basename='release')


urlpatterns = [
    path('', include(router.urls))
]
