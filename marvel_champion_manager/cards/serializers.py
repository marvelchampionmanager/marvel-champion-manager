from rest_framework import serializers
from cards.models import Hero, Scenario, EncounterSet, Release


class NameBasedRelatedField(serializers.HyperlinkedRelatedField):
    def display_value(self, instance):
        return instance.name


class ReleaseSerializer(serializers.HyperlinkedModelSerializer):
    heroes = NameBasedRelatedField(many=True, view_name='hero-detail', read_only=True)
    encounter_sets = NameBasedRelatedField(many=True, view_name='encounterset-detail', read_only=True)
    scenarios = NameBasedRelatedField(many=True, view_name='scenario-detail', read_only=True)

    class Meta:
        model = Release
        fields = ['url', 'id', 'name', 'release_date', 'standalone', 'heroes', 'encounter_sets',
                  'scenarios', 'cover_basename']


class HeroSerializer(serializers.HyperlinkedModelSerializer):
    release = NameBasedRelatedField(view_name='release-detail', queryset=Release.objects.all())

    class Meta:
        model = Hero
        fields = ['url', 'id', 'hero_name', 'alterego_name', 'release', 'cover_basename']


class EncounterSetSerializer(serializers.HyperlinkedModelSerializer):
    release = NameBasedRelatedField(view_name='release-detail', queryset=Release.objects.all())

    class Meta:
        model = EncounterSet
        fields = ['url', 'id', 'name', 'release', 'cover_basename']


class ScenarioSerializer(serializers.HyperlinkedModelSerializer):
    release = NameBasedRelatedField(view_name='release-detail', queryset=Release.objects.all())
    mandatory_encounter_sets = NameBasedRelatedField(many=True,
                                                     view_name='encounterset-detail',
                                                     queryset=EncounterSet.objects.all())
    recommended_encounter_sets = NameBasedRelatedField(many=True,
                                                       view_name='encounterset-detail',
                                                       queryset=EncounterSet.objects.all())

    class Meta:
        model = Scenario
        fields = ['url', 'id', 'name', 'encounter_set_number', 'mandatory_encounter_sets', 'recommended_encounter_sets',
                  'release', 'cover_basename']
