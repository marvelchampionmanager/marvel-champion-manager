from cards.models import Hero, EncounterSet, Scenario, Release
from cards.serializers import HeroSerializer, EncounterSetSerializer, ScenarioSerializer, ReleaseSerializer
from rest_framework import viewsets, permissions
from cards.permissions import IsAdminUserOrReadOnly


class ReleaseViewSet(viewsets.ModelViewSet):
    queryset = Release.objects.all()
    serializer_class = ReleaseSerializer
    permission_classes = [IsAdminUserOrReadOnly]
    # permission_classes = [permissions.IsAuthenticated, IsAdminUserOrReadOnly]


class HeroViewSet(viewsets.ModelViewSet):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer
    permission_classes = [IsAdminUserOrReadOnly]
    # permission_classes = [permissions.IsAuthenticated, IsAdminUserOrReadOnly]


class EncounterSetViewSet(viewsets.ModelViewSet):
    queryset = EncounterSet.objects.all()
    serializer_class = EncounterSetSerializer
    permission_classes = [IsAdminUserOrReadOnly]
    # permission_classes = [permissions.IsAuthenticated, IsAdminUserOrReadOnly]


class ScenarioViewSet(viewsets.ModelViewSet):
    queryset = Scenario.objects.all()
    serializer_class = ScenarioSerializer
    permission_classes = [IsAdminUserOrReadOnly]
    # permission_classes = [permissions.IsAuthenticated, IsAdminUserOrReadOnly]
