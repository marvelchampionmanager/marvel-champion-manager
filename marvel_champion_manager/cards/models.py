from django.db import models


class Release(models.Model):
    name = models.CharField(max_length=100, unique=True)
    release_date = models.DateField()
    standalone = models.BooleanField(default=True)
    cover_basename = models.CharField(max_length=20, unique=True)


class Hero(models.Model):
    hero_name = models.CharField(max_length=100)
    alterego_name = models.CharField(max_length=100)
    release = models.ForeignKey(Release, on_delete=models.RESTRICT)
    constraints = [
        models.UniqueConstraint(fields=['hero_name', 'alterego_name'],
                                name='unique_hero')
    ]
    cover_basename = models.CharField(max_length=20, unique=True)

    def __getattr__(self, item):
        if item == 'name':
            return str(self.hero_name) + ' aka ' + str(self.alterego_name)


class EncounterSet(models.Model):
    name = models.CharField(max_length=100, unique=True)
    release = models.ForeignKey(Release, on_delete=models.RESTRICT)
    cover_basename = models.CharField(max_length=20, unique=True)


class Scenario(models.Model):
    name = models.CharField(max_length=100, unique=True)
    encounter_set_number = models.IntegerField()
    mandatory_encounter_sets = models.ManyToManyField(
        'EncounterSet', related_name='mandatory_encounter_sets'
    )
    recommended_encounter_sets = models.ManyToManyField(
        'EncounterSet', related_name='recommended_encounter_sets'
    )
    release = models.ForeignKey(Release, on_delete=models.RESTRICT)
    cover_basename = models.CharField(max_length=20, unique=True)
